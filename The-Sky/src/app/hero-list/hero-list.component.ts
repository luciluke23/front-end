import { Component, OnInit } from '@angular/core';
import { HeroService } from '../hero.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.css']
})
export class HeroListComponent implements OnInit {
  heros = [];
  weather_detail = [];

  constructor
  (
    private data: HeroService,
    private router: Router  
  ) { }

  ngOnInit() {
    this.data.getData().subscribe(     
      data => {
        console.log(data);
        this.heros = data.list
        this.weather_detail = data.heros;
        console.log(this.heros);
        
      }
    );
  }
Clickme(e){
    this.data.setCurrentWeather(e);
    this.router.navigateByUrl('/details');
}
}





