import { Component, OnInit } from '@angular/core';
import { HeroService } from '../hero.service';
import { Observable } from 'rxjs';
import {Router } from "@angular/router";

@Component({
  selector: 'app-hero-details',
  templateUrl: './hero-details.component.html',
  styleUrls: ['./hero-details.component.css']
})
export class HeroDetailsComponent implements OnInit {
  heros = [];
  weather : any;

  constructor(
    private data: HeroService,
    private router : Router
    ) { }



    ngOnInit() {
      this.heros = this.data.getCurrentWeather();
      console.log(this.heros);
      
    }
  
    onclick()
    {
      this.router.navigateByUrl("/list");
    }
  
  }
  // ngOnInit() {
  //   this.data.getData().subscribe(     
  //     data => {
  //       console.log(data);
  //       this.heros = data.list
  //     }
  //   );
  // }








